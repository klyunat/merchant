** How to run **

mvn spring-boot:run - runs the server on localhost:8080
mvn test - runs test

** How to use **

1) create offer:
	POST localhost:8080/offer

   body example:
	
{
  "price": 1.2,
  "currency": "EUR",
  "merchant": {
    "id": 2,
    "name": "happy farmer"
  },
  "product": {
    "id": 123,
    "name": "apples",
    "description": "best appples"
  }
}

  response:
  
{
  "id": 1,
  "price": 1.2,
  "currency": "EUR",
  "merchant": {
    "id": 2,
    "name": "happy farmer"
  },
  "product": {
    "id": 123,
    "name": "apples",
    "description": "best appples"
  }
}

2) get offer:
	GET localhost:8080/offer/{offerId}
	
3) delete offer:
	DELETE localhost:8080/offer/{offerId}
