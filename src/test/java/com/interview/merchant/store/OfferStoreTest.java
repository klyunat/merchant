package com.interview.merchant.store;

import static org.junit.Assert.*;

import java.util.Currency;

import org.junit.Test;

import com.interview.merchant.data.Merchant;
import com.interview.merchant.data.Offer;
import com.interview.merchant.data.Product;

public class OfferStoreTest {

	@Test
	public void testEmpty() {
		OfferStore store = new OfferStore();
		assertNull(store.getOffer(123));
	}
	
	@Test
	public void testNegativeId() {
		OfferStore store = new OfferStore();
		assertNull(store.getOffer(-1));
	}
	
	@Test(expected = IllegalArgumentException.class)  
	public void testAddNull() {
		OfferStore store = new OfferStore();
		store.addOffer(null);
	}
	
	@Test(expected = IllegalArgumentException.class)  
	public void testNullMerchantId() {
		OfferStore store = new OfferStore();
		
		Offer offer = new Offer();
		offer.setPrice(1.2);
		offer.setCurrency(Currency.getInstance("GBP"));
		offer.setMerchant(new Merchant());
		offer.setProduct(new Product(5, "apple", "best apples ever"));
		
		store.addOffer(offer);
	}
	
	@Test(expected = IllegalArgumentException.class)  
	public void testNullMerchant() {
		OfferStore store = new OfferStore();
		
		Offer offer = new Offer();
		offer.setPrice(1.2);
		offer.setCurrency(Currency.getInstance("GBP"));
		offer.setProduct(new Product(5, "apple", "best apples ever"));
		
		store.addOffer(offer);
	}
	
	@Test(expected = IllegalArgumentException.class)  
	public void testNullProductId() {
		OfferStore store = new OfferStore();
		
		Offer offer = new Offer();
		offer.setPrice(1.2);
		offer.setCurrency(Currency.getInstance("GBP"));
		offer.setMerchant(new Merchant(1, "Happy Farmer"));
		offer.setProduct(new Product());
		
		store.addOffer(offer);
	}
	
	@Test(expected = IllegalArgumentException.class)  
	public void testNullProduct() {
		OfferStore store = new OfferStore();
		
		Offer offer = new Offer();
		offer.setPrice(1.2);
		offer.setCurrency(Currency.getInstance("GBP"));
		offer.setMerchant(new Merchant(1, "Happy Farmer"));
		
		store.addOffer(offer);
	}
	
	@Test
	public void testAddAndGet() {
		OfferStore store = new OfferStore();
				
		Offer offer = new Offer();
		offer.setPrice(1.2);
		offer.setCurrency(Currency.getInstance("GBP"));
		offer.setMerchant(new Merchant(1, "Happy Farmer"));
		offer.setProduct(new Product(5, "apple", "best apples ever"));
		
		Offer savedOffer = store.addOffer(offer);
		assertNotNull(savedOffer.getId());
		assertEquals(offer.getPrice(), savedOffer.getPrice(), 0.);
		assertEquals(offer.getCurrency(), savedOffer.getCurrency());
		assertEquals(offer.getProduct(), savedOffer.getProduct());
		assertEquals(offer.getMerchant(), savedOffer.getMerchant());

		
		Offer retrievedOffer = store.getOffer(savedOffer.getId());
		assertEquals(offer, retrievedOffer);
	}
	
	@Test
	public void testAddAndGetDifferent() {
		OfferStore store = new OfferStore();
				
		Offer offer = new Offer();
		offer.setPrice(1.2);
		offer.setCurrency(Currency.getInstance("GBP"));
		offer.setMerchant(new Merchant(1, "Happy Farmer"));
		offer.setProduct(new Product(5, "apple", "best apples ever"));
		
		offer = store.addOffer(offer);
		assertNotNull(offer.getId());
		
		assertNull(store.getOffer(offer.getId() + 100));
	}
	
	@Test
	public void testAddSeveral() {
		OfferStore store = new OfferStore();
				
		Offer offer1 = new Offer();
		offer1.setPrice(1.2);
		offer1.setCurrency(Currency.getInstance("GBP"));
		offer1.setMerchant(new Merchant(1, "Happy Farmer"));
		offer1.setProduct(new Product(5, "apple", "best apples ever"));
		offer1 = store.addOffer(offer1);
		assertNotNull(offer1.getId());
		
		Offer offer2 = new Offer();
		offer2.setPrice(0.5);
		offer2.setCurrency(Currency.getInstance("GBP"));
		offer2.setMerchant(new Merchant(2, "Angry Farmer"));
		offer2.setProduct(new Product(8, "pear", "worm-eaten pear"));
		offer2 = store.addOffer(offer2);
		assertNotNull(offer2.getId());
		
		assertNotEquals(offer1.getId(), offer2.getId());
		
		assertEquals(offer1, store.getOffer(offer1.getId()));
		assertEquals(offer2, store.getOffer(offer2.getId()));
	}
	
	@Test
	public void deleteUnknowd() {
		OfferStore store = new OfferStore();
		store.deleteOffer(123);
		// tests that no exception thrown
	}
	
	@Test
	public void testDelete() {
		OfferStore store = new OfferStore();
		
		Offer offer = new Offer();
		offer.setPrice(1.2);
		offer.setCurrency(Currency.getInstance("GBP"));
		offer.setMerchant(new Merchant(1, "Happy Farmer"));
		offer.setProduct(new Product(5, "apple", "best apples ever"));
		
		offer = store.addOffer(offer);
		
		int id = offer.getId();
		store.deleteOffer(id);
		assertNull(store.getOffer(id));
	}
	
}
