package com.interview.merchant;

import static org.assertj.core.api.Assertions.assertThat;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.interview.merchant.data.Offer;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class OfferWebServiceTest {
	
	private static String OFFER1 = "{\"price\":1.2,\"currency\":\"EUR\",\"merchant\":{\"id\":2,\"name\":\"happy farmer\"},\"product\":{\"id\":123,\"name\":\"apples\",\"description\":\"best appples\"}}";
	
	private TestRestTemplate restTemplate = new TestRestTemplate();
	
	@LocalServerPort
    private int randomServerPort;	
	
	private String getHost() {
		return "http://localhost:" + randomServerPort;
	}
	
	@Test
	public void testEmpty() {
		ResponseEntity<String> resp= restTemplate.getForEntity(getHost() + "/offer/123", String.class);
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void testAdd() throws JSONException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(OFFER1 ,headers);
		ResponseEntity<Offer> resp= restTemplate.postForEntity(getHost() + "/offer", request, Offer.class);
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
		
		Offer offer = resp.getBody();
		assertThat(offer).isNotNull();
		assertThat(offer.getId()).isPositive();
		assertThat(offer.getPrice()).isEqualTo(1.2);
		
		assertThat(offer.getCurrency()).isNotNull();
		assertThat(offer.getCurrency().getCurrencyCode()).isEqualTo("EUR");
		
		assertThat(offer.getMerchant()).isNotNull();
		assertThat(offer.getMerchant().getId()).isEqualTo(2);
		assertThat(offer.getMerchant().getName()).isEqualTo("happy farmer");


		assertThat(offer.getProduct()).isNotNull();
		assertThat(offer.getProduct().getId()).isEqualTo(123);
		assertThat(offer.getProduct().getName()).isEqualTo("apples");
		assertThat(offer.getProduct().getDescription()).isEqualTo("best appples");

	}
	
	@Test
	public void testAddGet() throws JSONException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(OFFER1 ,headers);
		ResponseEntity<Offer> resp= restTemplate.postForEntity(getHost() + "/offer", request, Offer.class);
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
		
		resp= restTemplate.getForEntity(getHost() + "/offer/" + resp.getBody().getId(), Offer.class);
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
		Offer offer = resp.getBody();
		assertThat(offer).isNotNull();
		assertThat(offer.getId()).isPositive();
		assertThat(offer.getPrice()).isEqualTo(1.2);
		
		assertThat(offer.getCurrency()).isNotNull();
		assertThat(offer.getCurrency().getCurrencyCode()).isEqualTo("EUR");
		
		assertThat(offer.getMerchant()).isNotNull();
		assertThat(offer.getMerchant().getId()).isEqualTo(2);
		assertThat(offer.getMerchant().getName()).isEqualTo("happy farmer");


		assertThat(offer.getProduct()).isNotNull();
		assertThat(offer.getProduct().getId()).isEqualTo(123);
		assertThat(offer.getProduct().getName()).isEqualTo("apples");
		assertThat(offer.getProduct().getDescription()).isEqualTo("best appples");
	}
	
	@Test
	public void testAddDelete() throws JSONException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(OFFER1 ,headers);
		ResponseEntity<Offer> resp= restTemplate.postForEntity(getHost() + "/offer", request, Offer.class);
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
		
		int id = resp.getBody().getId();
		restTemplate.delete(getHost() + "/offer/" + id);
		
		ResponseEntity<String> getResp = restTemplate.getForEntity(getHost() + "/offer/" + id, String.class);
		assertThat(getResp.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

}
