package com.interview.merchant.store;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.interview.merchant.data.Offer;

public class OfferStore implements IOfferStore {
	
	private AtomicInteger idGenerator = new AtomicInteger();
	
	private ConcurrentMap<Integer, Offer> offers = new ConcurrentHashMap<Integer, Offer>(); 
	
	@Override
	public Offer addOffer(Offer offer) {
		if(offer == null) {
			throw new IllegalArgumentException("Offer cannot be null");
		}
		
		if(offer.getMerchant() == null || offer.getMerchant().getId() <= 0) {
			throw new IllegalArgumentException("Merchant is not set");
		}
		
		if(offer.getProduct() == null || offer.getProduct().getId() <= 0) {
			throw new IllegalArgumentException("Product is not set");
		}
		
		Integer id = idGenerator.incrementAndGet();
		offer.setId(id);
		offers.put(id, offer);
		
		return offer;
	}

	@Override
	public Offer getOffer(int offerId) {
		return offers.get(offerId);
	}

	@Override
	public void deleteOffer(int offerId) {
		offers.remove(offerId);
	}

}
