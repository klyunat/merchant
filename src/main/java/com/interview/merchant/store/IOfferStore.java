package com.interview.merchant.store;

import com.interview.merchant.data.Offer;

public interface IOfferStore {
	
	Offer addOffer(Offer offer);
	
	Offer getOffer(int offerId);

	void deleteOffer(int offerId);

}
