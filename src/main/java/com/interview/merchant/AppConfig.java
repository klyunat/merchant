package com.interview.merchant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.interview.merchant.store.IOfferStore;
import com.interview.merchant.store.OfferStore;

@Configuration
public class AppConfig {
	
    @Bean
    public IOfferStore offerStore() {
        return new OfferStore();
    }
}
