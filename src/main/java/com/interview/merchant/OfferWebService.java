package com.interview.merchant;

import java.io.IOException;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.interview.merchant.data.Offer;
import com.interview.merchant.store.IOfferStore;

@RestController
@SpringBootApplication
public class OfferWebService {
	
	@Autowired
	private IOfferStore offerStore;
	
	@RequestMapping(value = "/offer", method = RequestMethod.POST)
	Offer addOffer(@RequestBody Offer offer) throws Exception {
		offerStore.addOffer(offer);
		return offer;
    }
	
	@RequestMapping(value = "/offer/{offerId}")
	Offer getOffer(@PathVariable("offerId") int offerId) {
		Offer result = offerStore.getOffer(offerId);
		if(result == null) {
			throw new NoSuchElementException();
		}
		return result;
    }
	
	@RequestMapping(value = "/offer/{offerId}", method = RequestMethod.DELETE)
	void deleteOffer(@PathVariable("offerId") int offerId) {
		offerStore.deleteOffer(offerId);
    }
	
	@Autowired
	public void setStore(IOfferStore store) {
		this.offerStore = store;
	}
	
	@ExceptionHandler
	void handleNoSuchElementException(NoSuchElementException e, HttpServletResponse response) throws IOException {
	    response.sendError(HttpStatus.NOT_FOUND.value());
	}
	
	@ExceptionHandler
	void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
		e.printStackTrace();
	    response.sendError(HttpStatus.BAD_REQUEST.value());
	}

    public static void main(String[] args) throws Exception {
        SpringApplication.run(OfferWebService.class, args);
    }
}
